package pl.e_kontrolarodzicielska.krtracker.activity;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;
import java.util.HashMap;

import pl.e_kontrolarodzicielska.krtracker.R;
import pl.e_kontrolarodzicielska.krtracker.utils.DatabaseMethods;

public class RegistrationActivity extends AppCompatActivity {

    private static String REGISTER_CHILD_ADDRESS = "RegisterChildInDatabase.php";
    private static final String PREFERENCE_NAME = "MyLastLocation";
    private String messageResponse;
    private JSONObject jsonResponse;
    private DatabaseMethods converter;
    private int successResponse;

    EditText etPassword;
    EditText etLogin;
    EditText etPhoneNumber;
    Button bRegister;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register_layout);
        initView();
        setListeners();
    }
    private void initView(){
        etPassword = (EditText) findViewById(R.id.etPassword);
        etLogin = (EditText) findViewById(R.id.etLogin);
        etPhoneNumber = (EditText) findViewById(R.id.etPhoneNumber);
        bRegister = (Button) findViewById(R.id.bRegisterChild);
    }
    private void setListeners(){
        bRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String login = etLogin.getText().toString().trim();
                String phone = etPhoneNumber.getText().toString().trim();
                String uid = SetUID(phone);
               // SetUID(phone);
                new RegisterChild().execute(login,phone,uid);
            }
        });
    }
    public String SetUID(String tel)
    {
        int date = (int) new Date().getTime();
        int tele = Integer.parseInt(tel.trim());
        String tel_tmp = Integer.toHexString(tele);
        String date_tmp = Integer.toHexString(date);
        String returnValue = (tel_tmp+date_tmp).substring(5,15);
        return returnValue;
    }

    class RegisterChild extends AsyncTask<String, String, String> {

        String uid;
        String login;
        String phoneNumber;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            messageResponse = "";
            jsonResponse = new JSONObject();
            converter = new DatabaseMethods();
            successResponse=0;
        }
        protected String doInBackground(String... args) {
            login = args[0];
            phoneNumber = args[1];
            uid = args[2];

            HashMap<String, String> params = new HashMap<>();
            params.put("uid_child", uid);
            params.put("name",login);
            params.put("phone",phoneNumber);
            jsonResponse = converter.PostMethodResult(params,REGISTER_CHILD_ADDRESS);
            try {
                successResponse = jsonResponse.getInt("success");
                messageResponse = jsonResponse.getString("message");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }
        protected void onPostExecute(String file_url) {

            runOnUiThread(new Runnable() {
                public void run() {
                    // TODO: 25.03.2017 "REGISTRATION"
                    // decide what to do with registration
                    // accept : go to another activity ?
                    // reject : enter data one more time ?

                    if(successResponse==1) {
                        SharedPreferences sharedPref = getPreferences(Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = sharedPref.edit();
                        editor.putString("uid", uid);
                        editor.putString("phone", phoneNumber);
                        editor.putString("login", login);
                        editor.apply();
                    }
                    Toast.makeText(getApplicationContext(),messageResponse, Toast.LENGTH_SHORT).show();
                }
            });
        }
    }
}
