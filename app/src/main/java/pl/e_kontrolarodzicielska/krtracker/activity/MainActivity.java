package pl.e_kontrolarodzicielska.krtracker.activity;

import android.Manifest;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.Date;

import pl.e_kontrolarodzicielska.krtracker.R;
import pl.e_kontrolarodzicielska.krtracker.service.BootCompletedIntentReceiver;

public class MainActivity extends AppCompatActivity {

    private static final String PREFERENCE_NAME = "MyLastLocation";

    private String uid;
    private String phone;
    private String login;

    Button bGeneratePassword;
    Button bSendByPasswordSMS;
    TextView tvTemporaryPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if(!isUidSet())
            goToRegistration();
        Trigger(uid);
        setContentView(R.layout.activity_main);
        initView();
        setListeners();
    }

    private void initView(){
        bGeneratePassword = (Button) findViewById(R.id.bGeneratePassword);
        bSendByPasswordSMS = (Button) findViewById(R.id.bSendPassBySMS);
        tvTemporaryPassword = (TextView) findViewById(R.id.tvTemporaryPassword);
    }

    private void setListeners(){
        bGeneratePassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tvTemporaryPassword.setText(setPass());
                bSendByPasswordSMS.setVisibility(View.VISIBLE);
            }
        });

        bSendByPasswordSMS.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
    }

    public String setPass() {
        int dateI = (int) new Date().getTime();
        int phoneI = Integer.parseInt(phone);
        String retValue = Integer.toHexString(phoneI+dateI);
        //Todo remove debug traces
        Log.d("####DEBUG: retValue ",retValue);
        //String date_tmp = Integer.toHexString(dateI);
        return retValue;
    }

    private Boolean isUidSet(){
        SharedPreferences sharedPref = getPreferences(Context.MODE_PRIVATE);
        uid = sharedPref.getString("uid", "undefined");
        phone = sharedPref.getString("phone", "123456789");
        login = sharedPref.getString("login", "undefined");
        if(uid.equals(""))
            return false;
        else
            return true;
    }

    public void Trigger(String uid) {
        Intent intent = new Intent(getApplicationContext(), BootCompletedIntentReceiver.class);
        intent.putExtra("uid",uid);

        final PendingIntent pIntent = PendingIntent.getBroadcast(getApplicationContext(), 0,
                intent, PendingIntent.FLAG_UPDATE_CURRENT);
        long firstMillis = System.currentTimeMillis();
        AlarmManager alarm = (AlarmManager) getSystemService(ALARM_SERVICE);

        alarm.setInexactRepeating(AlarmManager.RTC, firstMillis,1000*30, pIntent);

    }

    private void goToRegistration(){
        Intent registrationActivity = new Intent(getApplicationContext(),RegistrationActivity.class);
        startActivity(registrationActivity);
    }
}
