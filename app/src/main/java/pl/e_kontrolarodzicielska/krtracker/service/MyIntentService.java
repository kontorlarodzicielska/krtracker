package pl.e_kontrolarodzicielska.krtracker.service;

import android.app.Activity;
import android.app.IntentService;
import android.content.Intent;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;

import pl.e_kontrolarodzicielska.krtracker.utils.DatabaseMethods;
import pl.e_kontrolarodzicielska.krtracker.utils.GPSLocation;

/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p/>
 * TODO: Customize class - update intent actions, extra parameters and static
 * helper methods.
 */
public class MyIntentService extends IntentService {

    private static final String PREFERENCE_NAME = "MyLastLocation";
    private static final String ADD_COORD_ADDRESS = "AddLatLon.php";
    DatabaseMethods convert;
    GPSLocation gpsLocation;
    Double lastLon;
    Double lastLat;

    public MyIntentService() {
        super("MyIntentService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        String uid = intent.getStringExtra("uid");
        convert = new DatabaseMethods();
        gpsLocation = new GPSLocation(MyIntentService.this);
        getLastKnowPosition();

        Double lon = gpsLocation.getLongitude();
        Double lat = gpsLocation.getLatitude();
        DateFormat df1 = new SimpleDateFormat("HH:mm", Locale.getDefault());
        DateFormat df2 = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        String time = df1.format(Calendar.getInstance().getTime());
        String date = df2.format(Calendar.getInstance().getTime());
        if(isOnline() && isValidToSend(lat,lon)) {
            Log.d("SEND:",time+" "+date);
            HashMap<String, String> params = new HashMap<>();
            params.put("uid", uid);
            params.put("lat", lat.toString());
            params.put("lon", lon.toString());
            params.put("time", time);
            params.put("date", date);
            saveLastKnowPosition(lat,lon);
            convert.PostMethodResult(params, ADD_COORD_ADDRESS);
        }
    }
    private boolean isOnline() {
        ConnectivityManager cm =
                (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null && netInfo.isConnected();
    }

    private void getLastKnowPosition(){
        SharedPreferences pref = getApplication().getSharedPreferences(PREFERENCE_NAME, Activity.MODE_PRIVATE);
        lastLon = Double.parseDouble(pref.getString("lon","0.0"));
        lastLat = Double.parseDouble(pref.getString("lat","0.0"));
        //Todo: clear debug
        Log.d("DEBUG: ",pref.getString("lon","0.0")+" "+pref.getString("lat","0.0"));
    }
    private void saveLastKnowPosition(Double lat,Double lon){
        SharedPreferences sharedPref = getApplicationContext().getSharedPreferences(PREFERENCE_NAME, Activity.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString("lat", lat.toString());
        editor.putString("lon", lon.toString());
        editor.apply();
    }

    private boolean isValidToSend(Double lat,Double lon){
        if(getDistance(lat,lon,lastLat,lastLon) > 50 ) {
            return true;
        }else {
            return false;
        }
    }

    private Double getDistance(Double lat, Double lon, Double lastLat, Double lastLon) {
        Double earthRadius = 6371000.0; //meters
        Double dLat = Math.toRadians(lastLat-lat);
        Double dLng = Math.toRadians(lastLon-lon);
        Double a = Math.sin(dLat/2) * Math.sin(dLat/2) +
                Math.cos(Math.toRadians(lat)) * Math.cos(Math.toRadians(lastLat)) *
                        Math.sin(dLng/2) * Math.sin(dLng/2);
        Double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
        Double dist = (earthRadius * c);

        //todo clear debug
        Log.d("##DEBUG DISTANCE: ", dist.toString());
        return dist;
    }
}
