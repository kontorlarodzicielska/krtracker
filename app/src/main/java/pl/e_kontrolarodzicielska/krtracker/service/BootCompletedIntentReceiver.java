package pl.e_kontrolarodzicielska.krtracker.service;

/**
 * Created by Użytkownik on 27.03.2017.
 */
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class BootCompletedIntentReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {

        String uid = intent.getStringExtra("uid");
        Intent i = new Intent(context, MyIntentService.class);
        i.putExtra("uid", uid);
        context.startService(i);

    }
}
