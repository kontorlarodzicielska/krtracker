package  pl.e_kontrolarodzicielska.krtracker.utils;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

/**
 * Created by Użytkownik on 18.12.2016.
 */
public class DatabaseMethods {

    // ścieżka do serwera z plikami php
    final static protected String SERVER_ADDRES = "http://e-kontrolarodzicielska.pl/php/";
    String result;

    public DatabaseMethods() // default cstr
    {
        result="";
    }

    // Metoda POST przesyłania danych
    public JSONObject PostMethodResult(HashMap<String,String> param, String adres) {
        String Addres="";
        JSONObject json= null;
        try {
            // złożenie stałego adresu oraz nazwy pliku php.
            Addres = SERVER_ADDRES +adres;
            URL c = new URL(Addres);
            // otworzenie połączenia oraz ustawienie podstawowych parametrów jak timeout, odczyt, zapis
            HttpURLConnection conn = (HttpURLConnection) c.openConnection();
            conn.setReadTimeout(15000);
            conn.setConnectTimeout(15000);
            conn.setRequestMethod("POST");
            conn.setDoInput(true);
            conn.setDoOutput(true);
            // przekazanie parametrów potrzebnych do przetworzenia zapytania SQL
            OutputStream os = conn.getOutputStream();
            BufferedWriter writer = new BufferedWriter(
                    new OutputStreamWriter(os, "UTF-8"));
            writer.write(getPostDataString(param));
            writer.flush();
            writer.close();
            os.close();
            //Sprawdzenie czy doszło do połączenia
            Integer responseCode = conn.getResponseCode();
            if (responseCode == HttpsURLConnection.HTTP_OK) {
                // zapisanie requesta do zmiennej result
                String line;
                BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                while ((line = br.readLine()) != null) {
                    result += line;
                }
            } else {
                result = responseCode.toString();
            }

            try {
                //parsowanie rezultatu do JSONa
                json = new JSONObject(result);
            } catch (JSONException e) {
                e.printStackTrace();
            }

        } catch (IOException e) {
        }
        return json;
    }



    // Metoda GET przesyłania danych
    public JSONObject GetMethodResult(String param, String adres) {

        String Addres="";
        JSONObject json= null;
        try {
            //Laczenie adresu serwera i pliku, oraz parametrów w jeden adres
            Addres = SERVER_ADDRES +adres;
            //String getParmString = param.toString();
            //getParmString = getParmString.substring(1,getParmString.length()-1);
            Addres = Addres +"?"+ param;
            //Addres = Addres.replace(", ","&");
            URL c = new URL(Addres);
            Log.d("#####DEBUG: ",c.toString());
            //-----------------------------------------------------------//
            HttpURLConnection conn = (HttpURLConnection) c.openConnection();
            conn.setReadTimeout(15000);
            conn.setConnectTimeout(15000);
            conn.setRequestMethod("GET");
            conn.setDoInput(true);
            conn.setDoOutput(true);


            Integer responseCode = conn.getResponseCode();
            if (responseCode == HttpsURLConnection.HTTP_OK) {
                String line;
                BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                while ((line = br.readLine()) != null) {
                    result += line;
                }
            } else {
                result = responseCode.toString();
            }
            try {
                json = new JSONObject(result);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } catch (IOException e) {
        }
        return json;
    }


    private String getPostDataString(HashMap<String, String> params) throws UnsupportedEncodingException {
        StringBuilder result = new StringBuilder();
        boolean first = true;
        for(Map.Entry<String, String> entry : params.entrySet()){
            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(entry.getKey(), "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(entry.getValue(), "UTF-8"));
        }

        return result.toString();
    }
    //TESTOWE FUNKCJE ZWRACAJĄCE STRING REQUESTA Z BAZY DANYCH

    public String GetMethodResult2(HashMap<String,String> param, String adres) {

        String Addres="";
        try {
            Addres = SERVER_ADDRES +adres;
            String getParmString = param.toString();
            getParmString = getParmString.substring(1,getParmString.length()-1);
            Addres = Addres +"?"+ getParmString;
            Addres = Addres.replace(", ","&");
            URL c = new URL(Addres);
            HttpURLConnection conn = (HttpURLConnection) c.openConnection();
            conn.setReadTimeout(15000);
            conn.setConnectTimeout(15000);
            conn.setRequestMethod("GET");
            conn.setDoInput(true);
            conn.setDoOutput(true);


            Integer responseCode = conn.getResponseCode();
            if (responseCode == HttpsURLConnection.HTTP_OK) {
                String line;
                BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                while ((line = br.readLine()) != null) {
                    result += line;
                }
            } else {
                result = responseCode.toString();

            }

        } catch (IOException e) {
        }
        return result;
    }


    public String PostMethodResult2(HashMap<String,String> param, String adres) {

        String Addres="";
        try {
            Addres = SERVER_ADDRES +adres;
            URL c = new URL(Addres);
            HttpURLConnection conn = (HttpURLConnection) c.openConnection();
            conn.setReadTimeout(15000);
            conn.setConnectTimeout(15000);
            conn.setRequestMethod("POST");
            conn.setDoInput(true);
            conn.setDoOutput(true);

            OutputStream os = conn.getOutputStream();
            BufferedWriter writer = new BufferedWriter(
                    new OutputStreamWriter(os, "UTF-8"));
            writer.write(getPostDataString(param));

            writer.flush();
            writer.close();
            os.close();
            Integer responseCode = conn.getResponseCode();

            if (responseCode == HttpsURLConnection.HTTP_OK) {
                String line;
                BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                while ((line = br.readLine()) != null) {
                    result += line;
                }
            } else {
                result = responseCode.toString();

            }

        } catch (IOException e) {
        }
        return result;
    }
    public String SetUID(String phone)
    {
        int date = (int) new Date().getTime();
        int tele = Integer.parseInt(phone);
        String tel_tmp = Integer.toHexString(tele);
        String date_tmp = Integer.toHexString(date);
        return (tel_tmp+date_tmp).substring(5,15);
    }
}